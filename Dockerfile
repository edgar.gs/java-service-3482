FROM gradle:5.1.1-jdk8-alpine as builder
WORKDIR /app
COPY build.gradle .
COPY src ./src
USER root
RUN gradle build


FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim
COPY --from=builder /app/build/libs/*.jar java-service-3482.jar
CMD java  -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar java-service-3482.jar
