package com.gs.micronaut.service;

import com.gs.micronaut.model.ClientRest;

import javax.inject.Singleton;

@Singleton
public class SimpleRestService {

    public ClientRest save(ClientRest input) {
        long id = (long)(Math.random()*100);
        input.setId(id);
        System.out.println("res:"+input);
        return input;
    }
}