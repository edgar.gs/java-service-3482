package com.gs.micronaut.controller;

import com.gs.micronaut.service.SimpleRestService;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;

import com.gs.micronaut.model.ClientRest;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;

@Controller("/simpleRest")
public class SimpleRestController {

    private SimpleRestService simpleRestService;

    public SimpleRestController(SimpleRestService simpleRestService){
        this.simpleRestService = simpleRestService;
    }

    @Get("/")
    public HttpStatus index() {
        return HttpStatus.OK;
    }

    @Post("/")
    public HttpResponse<ClientRest> save(ClientRest input){
        ClientRest clientRest = simpleRestService.save(input);
        return HttpResponse.created(clientRest);
    }
}